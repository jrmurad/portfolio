# NYSE - Coding Challenge - Part 1

[Requirement Specs PDF](NYSE_Coding_Challenge_20161013.pdf)

![Imgur](http://i.imgur.com/VDby70M.png)

## `npm install`

Installs dependencies.

## `npm start`

Runs the app in the development mode.

Open [http://localhost:3000](http://localost:3000) to view it in the browser.

## `npm test`

Runs the server tests with a test.sqlite database.

## `npm run test-client`

Runs the client tests.

## Tech Stack

* Node.js
* Express
* SQLite
* Sequelize
* create-react-app
* React
* Redux
* redux-saga
* Bootstrap
* Mocha
* Chai
* Enzyme

## TODO

* atomicize count+insert
* saga tests
* use SCSS
* move FLASH_DURATION and QUOTE_REFRESH_INTERVAL to config
