import chai from 'chai';
import chaiHttp from 'chai-http';
import config from 'config';

import db from '../server/lib/db';
import server from '../server';

chai.should();
chai.use(chaiHttp);

beforeEach((done) => {
  db.Stock.truncate().then(() => done());
});

describe('/GET portfolio', () => {
  it('should GET all the stocks', (done) => {
    db.Stock
      .create({ symbol: 'SYM' })
      .then(() => {
        chai.request(server)
          .get('/api/portfolio')
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('array');
            res.body.length.should.be.eql(1);
            done();
          });
      });
  });
});

describe('/POST/portfolio/add stock', () => {
  it('should POST a stock', (done) => {
    chai.request(server)
      .post('/api/portfolio/add')
      .send({ symbol: 'SYM' })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.stock.should.have.property('symbol');
        done();
      });
  });

  it('should reject when full', (done) => {
    const symbols = Array(config.maxPortfolioEntries).fill().map((x, i) => `SYM${i}`);

    Promise
      .all(symbols.map(symbol => db.Stock.create({ symbol })))
      .then(() => {
        chai.request(server)
          .post('/api/portfolio/add')
          .send({ symbol: 'SYM' })
          .end((err, res) => {
            res.should.have.status(500);
            done();
          });
      });
  });

  it('should reject duplicate symbols', (done) => {
    db.Stock
      .create({ symbol: 'SYM' })
      .then(() => {
        chai.request(server)
          .post('/api/portfolio/add')
          .send({ symbol: 'SYM' })
          .end((err, res) => {
            res.should.have.status(500);
            done();
          });
      });
  });
});

describe('/DELETE/portfolio/remove stock', () => {
  it('should DELETE a stock', (done) => {
    db.Stock
      .create({ symbol: 'SYM' })
      .then(({ id }) => {
        chai.request(server)
          .delete('/api/portfolio/remove')
          .send({ id })
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.have.property('n').eql(1);
            done();
          });
      });
  });
});
