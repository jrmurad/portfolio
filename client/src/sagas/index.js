import { delay, takeLatest } from 'redux-saga';
import { call, put } from 'redux-saga/effects';

import { clearNotification, displayNotification, flashError, flashInfo } from '../actions/notification';
import { receivePortfolio } from '../actions/portfolio';
import { receiveQuotes, requestQuotes } from '../actions/quotes';

const FLASH_DURATION = 3000;

function* addStock({ numSharesOwned, symbol }) {
  try {
    const response = yield fetch('/api/portfolio/add', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        numSharesOwned,
        symbol,
      }),
    });

    const json = yield response.json();

    if (!response.ok) {
      yield put(flashError(json.error));
    } else {
      yield put(requestQuotes([json.stock.symbol]));
      yield put({ type: 'APPEND_STOCK', stock: json.stock });
    }
  } catch (error) {
    yield put(flashError('error adding stock'));
  }
}

function* getPortfolio() {
  try {
    const response = yield fetch('/api/portfolio');
    const portfolio = yield response.json();

    yield put(requestQuotes(portfolio.map(stock => stock.symbol)));
    yield put(receivePortfolio(portfolio));
  } catch (error) {
    yield put(flashError('failed to fetch portfolio'));
  }
}

function* getQuotes({ symbols }) {
  if (!symbols.length) {
    return;
  }

  try {
    const response = yield fetch(`https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quote%20where%20symbol%20in%20(%22${symbols.join()}%22)&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys`);
    const json = yield response.json();
    const quotes = json.query.results.quote;

    yield put(receiveQuotes(Array.isArray(quotes) ? quotes : [quotes]));

    return quotes;
  } catch (error) {
    yield put(flashError('failed to fetch quotes'));
  }

  return [];
}

function* flashNotification(action) {
  yield put(displayNotification(action));
  yield delay(FLASH_DURATION);
  yield put(clearNotification());
}

function* flashQuickQuote({ symbol }) {
  const quote = yield call(getQuotes, { symbols: [symbol] });

  if (quote.LastTradePriceOnly) {
    yield put(flashInfo(`${quote.Symbol} ${quote.LastTradePriceOnly}`));
  } else {
    yield put(flashError(`no data for ${symbol}`));
  }
}

function* removeStock({ id }) {
  try {
    const response = yield fetch('/api/portfolio/remove', {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ id }),
    });

    const json = yield response.json();

    if (!response.ok) {
      yield put(flashError(json.error));
    } else {
      yield put({ type: 'DROP_STOCK', id });
    }
  } catch (error) {
    yield put(flashError('error removing stock'));
  }
}

function* sagas() {
  yield [
    takeLatest('ADD_STOCK', addStock),
    takeLatest('FLASH_NOTIFICATION', flashNotification),
    takeLatest('FLASH_QUICK_QUOTE', flashQuickQuote),
    takeLatest('GET_PORTFOLIO', getPortfolio),
    takeLatest('GET_QUOTES', getQuotes),
    takeLatest('REMOVE_STOCK', removeStock),
  ];
}

export default sagas;
