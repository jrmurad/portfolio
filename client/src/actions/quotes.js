export function flashQuickQuote(symbol) {
  return {
    type: 'FLASH_QUICK_QUOTE',
    symbol,
  };
}

export function receiveQuotes(quotes) {
  return {
    type: 'RECEIVE_QUOTES',
    quotes,
  };
}

export function requestQuotes(symbols) {
  return {
    type: 'GET_QUOTES',
    symbols,
  };
}

