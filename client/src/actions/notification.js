export function clearNotification() {
  return {
    type: 'CLEAR_NOTIFICATION',
  };
}

export function displayNotification({ bsStyle, message }) {
  return {
    type: 'DISPLAY_NOTIFICATION',
    bsStyle,
    message,
  };
}

export function flashError(message) {
  return {
    type: 'FLASH_NOTIFICATION',
    bsStyle: 'danger',
    message,
  };
}

export function flashInfo(message) {
  return {
    type: 'FLASH_NOTIFICATION',
    bsStyle: 'info',
    message,
  };
}
