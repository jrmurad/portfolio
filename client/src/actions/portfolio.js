export function addStock(symbol, numSharesOwned) {
  return {
    type: 'ADD_STOCK',
    numSharesOwned,
    symbol,
  };
}
export function receivePortfolio(portfolio) {
  return {
    type: 'RECEIVE_PORTFOLIO',
    portfolio,
  };
}

export function requestPortfolio() {
  return {
    type: 'GET_PORTFOLIO',
  };
}

export function removeStock(id) {
  return {
    type: 'REMOVE_STOCK',
    id,
  };
}
