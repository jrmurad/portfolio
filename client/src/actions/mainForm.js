export function setSymbol(symbol) { // eslint-disable-line import/prefer-default-export
  return {
    type: 'SET_SYMBOL',
    symbol,
  };
}
