import React, { Component } from 'react';
import { connect } from 'react-redux';

import { requestPortfolio } from '../actions/portfolio';
import { requestQuotes } from '../actions/quotes';

import Portfolio from '../components/Portfolio';

const QUOTE_REFRESH_INTERVAL = 5000;

const getStocksWithQuotes = ({ portfolio, quotes }) =>
  portfolio.stocks
    .map((stock) => {
      const quote = quotes[stock.symbol];

      if (!quote) {
        return stock;
      }

      return Object.assign({
        lastTick: quote.lastTick,
        name: quote.Name,
        price: Number(quote.LastTradePriceOnly) || null,
      }, stock);
    })
    .map(stock => Object.assign({ ...stock,
      numSharesOwned: Number(stock.numSharesOwned) || null, // sometimes empty str
    }))
    .sort((a, b) => (a.name || '').localeCompare(b.name));

const mapStateToProps = state => ({
  isLoading: state.portfolio.isFetching,
  stocks: getStocksWithQuotes(state),
});

class PortfolioContainer extends Component {
  componentWillMount() {
    // kick off initial load from server
    this.props.dispatch(requestPortfolio());

    // periodically update quotes
    this.interval = setInterval(() => {
      if (this.props.stocks.length) {
        this.props.dispatch(requestQuotes(this.props.stocks.map(stock => stock.symbol)));
      }
    }, QUOTE_REFRESH_INTERVAL);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return <Portfolio {...this.props} />;
  }
}

PortfolioContainer.propTypes = {
  dispatch: React.PropTypes.func,
  stocks: React.PropTypes.arrayOf(React.PropTypes.shape({
    symbol: React.PropTypes.string.isRequired,
    numSharesOwned: React.PropTypes.number,
  })),
};

export default connect(mapStateToProps)(PortfolioContainer);
