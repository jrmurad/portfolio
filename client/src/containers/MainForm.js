import { connect } from 'react-redux';

import { addStock } from '../actions/portfolio';
import { flashQuickQuote } from '../actions/quotes';
import { setSymbol } from '../actions/mainForm';

import MainForm from '../components/MainForm';

const mapStateToProps = state => ({
  symbol: state.mainForm.symbol,
});

export default connect(mapStateToProps, {
  onAddStock: (symbol, numSharesOwnedInput) => addStock(symbol, numSharesOwnedInput),
  onChangeSymbol: symbol => setSymbol(symbol.trim().toUpperCase()),
  onQuickQuote: symbol => flashQuickQuote(symbol),
})(MainForm);
