import { connect } from 'react-redux';

import { removeStock } from '../actions/portfolio';

import RemoveStock from '../components/RemoveStock';

const mapDispatchToProps = (dispatch, { id }) => ({
  onClick: (e) => {
    e.preventDefault();
    dispatch(removeStock(id));
  },
});

export default connect(null, mapDispatchToProps)(RemoveStock);
