import { connect } from 'react-redux';

import Notification from '../components/Notification';

const mapStateToProps = ({ notification }) => ({
  bsStyle: notification.bsStyle,
  message: notification.message,
});

export default connect(mapStateToProps)(Notification);
