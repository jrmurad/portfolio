import React from 'react';

import { Button, ControlLabel, Form, FormControl, FormGroup } from 'react-bootstrap';

import './MainForm.css';

const MainForm = ({ onAddStock, onChangeSymbol, onQuickQuote, symbol }) => {
  let numSharesOwnedInput;
  let symbolInput;

  return (
    <Form
      className="main-form text-center"
      inline
      onSubmit={(e) => {
        e.preventDefault();
        onQuickQuote(symbol);
        symbolInput.select();
      }}
    >
      <FormGroup controlId="symbol">
        <ControlLabel>Symbol</ControlLabel>
        {' '}
        <FormControl
          id="symbol"
          label="Symbol"
          inputRef={(node) => {
            symbolInput = node;
          }}
          onChange={(e) => {
            e.preventDefault();
            onChangeSymbol(e.currentTarget.value);
          }}
          value={symbol}
          autoComplete="off"
          autoFocus
        />
      </FormGroup>

      <FormGroup controlId="numSharesOwned">
        <ControlLabel># Shares</ControlLabel>
        {' '}
        <FormControl
          id="numSharesOwned"
          label="# Shares"
          type="number"
          inputRef={(node) => {
            numSharesOwnedInput = node;
          }}
        />
      </FormGroup>

      <Button
        className="add-to-portfolio"
        disabled={!symbol}
        onClick={(e) => {
          e.preventDefault();

          if (!symbol) {
            return;
          }

          onChangeSymbol('');
          onAddStock(symbol, numSharesOwnedInput.value);

          numSharesOwnedInput.value = '';

          symbolInput.select();
        }}
      >
        Add To Portfolio
      </Button>

      <Button
        bsStyle="primary"
        disabled={!symbol}
        type="submit"
      >
        Quick Quote
      </Button>
    </Form>
  );
};

MainForm.propTypes = {
  onAddStock: React.PropTypes.func.isRequired,
  onChangeSymbol: React.PropTypes.func.isRequired,
  onQuickQuote: React.PropTypes.func.isRequired,
  symbol: React.PropTypes.string,
};

export default MainForm;
