import React from 'react';
import { shallow } from 'enzyme';

import PriceMovement from './PriceMovement';

it('undefined movement', () => {
  const priceMovement = shallow(<PriceMovement />);
  expect(priceMovement.text()).toEqual('');
});

it('zero movement', () => {
  const priceMovement = shallow(<PriceMovement lastTick={0} />);
  expect(priceMovement.text()).toEqual('');
  expect(priceMovement.hasClass('down')).toBe(false);
  expect(priceMovement.hasClass('up')).toBe(false);
});

it('negative movement', () => {
  const priceMovement = shallow(<PriceMovement lastTick={-1} />);
  expect(priceMovement.text()).toEqual('↓');
  expect(priceMovement.hasClass('down')).toBe(true);
  expect(priceMovement.hasClass('up')).toBe(false);
});

it('positive movement', () => {
  const priceMovement = shallow(<PriceMovement lastTick={1} />);
  expect(priceMovement.hasClass('up')).toBe(true);
  expect(priceMovement.hasClass('down')).toBe(false);
});

