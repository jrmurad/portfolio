import React from 'react';
import { render } from 'enzyme';

import Notification from './Notification';

it('does not display when there is no message', () => {
  const alert = render(<Notification />).find('.alert');
  expect(alert.hasClass('fade')).toBe(true);
  expect(alert.hasClass('in')).toBe(false);
});

it('does display when there is a message', () => {
  const alert = render(<Notification message="message" />).find('.alert');
  expect(alert.hasClass('fade')).toBe(true);
  expect(alert.hasClass('in')).toBe(true);
});

it('contains message text', () => {
  expect(render(<Notification message="message" />).text()).toBe('message');
});
