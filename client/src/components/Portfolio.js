import React from 'react';

import { Table } from 'react-bootstrap';

import Loader from 'react-loader-advanced';

import PriceMovement from './PriceMovement';
import RemoveStock from '../containers/RemoveStock';

const getRows = stocks =>
  stocks.map(stock =>
    <tr key={stock.symbol}>
      <td>{stock.name}</td>
      <td>{stock.symbol}</td>
      <td>
        {stock.price}
        {' '}
        <PriceMovement lastTick={stock.lastTick} />
      </td>
      <td>{stock.numSharesOwned}</td>
      <td>{stock.numSharesOwned && (stock.numSharesOwned * stock.price).toFixed(2)}</td>

      <td className="text-center"><RemoveStock id={stock.id} /></td>
    </tr> // eslint-disable-line comma-dangle
  );

const Portfolio = ({ isLoading, stocks }) => (
  <Loader show={isLoading}>
    <Table striped bordered condensed hover>
      <thead>
        <tr>
          <th>Name</th>
          <th>Symbol</th>
          <th>Last Trade Price</th>
          <th># Shares Owned</th>
          <th>Market Value of Stake</th>

          <th />
        </tr>
      </thead>

      <tbody>
        {getRows(stocks)}
      </tbody>
    </Table>
  </Loader>
);

Portfolio.propTypes = {
  isLoading: React.PropTypes.bool,
  stocks: React.PropTypes.arrayOf(React.PropTypes.shape({
    lastTick: React.PropTypes.number,
    name: React.PropTypes.string,
    numSharesOwned: React.PropTypes.number,
    price: React.PropTypes.number,
    symbol: React.PropTypes.string,
  })),
};

export default Portfolio;
