import React from 'react';
import { render, shallow } from 'enzyme';
import sinon from 'sinon';

import RemoveStock from './RemoveStock';

it('contains button text', () => {
  expect(render(<RemoveStock onClick={() => {}} />).text()).toBe('Remove');
});

it('simulates click events', () => {
  const onClick = sinon.spy();
  const removeStock = shallow(<RemoveStock onClick={onClick} />);
  removeStock.simulate('click');
  expect(onClick.callCount).toBe(1);
});
