import React from 'react';

import MainForm from '../containers/MainForm';
import Notification from '../containers/Notification';
import Portfolio from '../containers/Portfolio';

const App = () =>
  <div className="App">
    <MainForm />
    <Notification />
    <Portfolio />
  </div>;

export default App;
