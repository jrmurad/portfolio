import React from 'react';

import './PriceMovement.css';

const PriceMovement = ({ lastTick }) => {
  if (lastTick < 0) {
    return <span className="price-movement down">↓</span>;
  } else if (lastTick > 0) {
    return <span className="price-movement up">↑</span>;
  }

  return null;
};

PriceMovement.propTypes = {
  lastTick: React.PropTypes.number,
};

export default PriceMovement;
