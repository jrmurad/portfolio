import React from 'react';
import { mount } from 'enzyme';
import sinon from 'sinon';

import MainForm from './MainForm';

const props = {
  onAddStock: () => {},
  onChangeSymbol: () => {},
  onQuickQuote: () => {},
};

it('changes symbol', () => {
  const callback = sinon.spy();

  const mainForm = mount(<MainForm {...props} onChangeSymbol={callback} />);
  const symbolInput = mainForm.find('#symbol');

  symbolInput.node.value = 'sym';
  symbolInput.simulate('change', symbolInput);

  expect(callback.calledWith('sym')).toBe(true);
});

it('requests quick quote on submit', () => {
  const callback = sinon.spy();

  const mainForm = mount(<MainForm {...props} onQuickQuote={callback} symbol="sym" />);

  mainForm.simulate('submit');

  expect(callback.calledWith('sym')).toBe(true);
});

it('adds stock to portfolio when number of shares is unspecified', () => {
  const callback = sinon.spy();

  const mainForm = mount(<MainForm {...props} onAddStock={callback} symbol="sym" />);
  const addToPortfolioBtn = mainForm.find('.add-to-portfolio');

  addToPortfolioBtn.simulate('click');

  expect(callback.calledWith('sym', '')).toBe(true);
});

it('adds stock with number of shares to portfolio', () => {
  const callback = sinon.spy();

  const mainForm = mount(<MainForm {...props} onAddStock={callback} symbol="sym" />);
  const addToPortfolioBtn = mainForm.find('.add-to-portfolio');

  mainForm.find('#numSharesOwned').node.value = '100';

  addToPortfolioBtn.simulate('click');

  expect(callback.calledWith('sym', '100')).toBe(true);
});
