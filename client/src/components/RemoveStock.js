import React from 'react';

import { Button } from 'react-bootstrap';

const RemoveStock = ({ onClick }) =>
  <Button bsStyle="danger" onClick={onClick}>
    Remove
  </Button>;

RemoveStock.propTypes = {
  onClick: React.PropTypes.func.isRequired,
};

export default RemoveStock;
