import React from 'react';

import { Alert, Fade } from 'react-bootstrap';

import './Notification.css';

const Notification = ({ message, bsStyle }) =>
  <Fade in={!!message}>
    <Alert bsStyle={bsStyle} className="notification">
      {message}
    </Alert>
  </Fade>;

Notification.propTypes = {
  bsStyle: React.PropTypes.oneOf(['danger', 'info']),
  message: React.PropTypes.string,
};

export default Notification;
