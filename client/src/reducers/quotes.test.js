import reducer from './quotes';

it('should return the initial state', () => {
  expect(reducer(undefined, {})).toEqual({});
});

it('should handle RECEIVE_QUOTES', () => {
  expect(
    reducer({
      SYM1: { Symbol: 'SYM1', LastTradePriceOnly: 101 },
      SYM2: { Symbol: 'SYM2', LastTradePriceOnly: 100 },
      SYM3: { Symbol: 'SYM3', LastTradePriceOnly: 100 },
      SYM4: { Symbol: 'SYM4' },
      SYM5: { Symbol: 'SYM5' },
    }, {
      type: 'RECEIVE_QUOTES',
      quotes: [
        { Symbol: 'SYM1', LastTradePriceOnly: 100 }, // down -1
        { Symbol: 'SYM2', LastTradePriceOnly: 101 }, // up +1
        { Symbol: 'SYM3', LastTradePriceOnly: 100 }, // unchanged
        { Symbol: 'SYM4', LastTradePriceOnly: 100 }, // no prev price
     // { Symbol: 'SYM5' }, // absent
        { Symbol: 'SYM6' }, // new addition
      ],
    })
  ).toEqual({
    SYM1: { Symbol: 'SYM1', LastTradePriceOnly: 100, lastTick: -1 },
    SYM2: { Symbol: 'SYM2', LastTradePriceOnly: 101, lastTick: 1 },
    SYM3: { Symbol: 'SYM3', LastTradePriceOnly: 100, lastTick: 0 },
    SYM4: { Symbol: 'SYM4', LastTradePriceOnly: 100, lastTick: NaN },
    SYM5: { Symbol: 'SYM5' },
    SYM6: { Symbol: 'SYM6', lastTick: undefined },
  });
});
