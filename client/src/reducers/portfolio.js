export default function portfolio(state = {
  isFetching: true,
  stocks: [],
}, action) {
  switch (action.type) {
    case 'APPEND_STOCK':
      return Object.assign({}, state, {
        stocks: state.stocks.concat(action.stock),
      });
    case 'RECEIVE_PORTFOLIO':
      return Object.assign({}, state, {
        isFetching: false,
        stocks: action.portfolio,
      });
    case 'DROP_STOCK':
      return Object.assign({}, state, {
        stocks: state.stocks.filter(stock => stock.id !== action.id),
      });
    case 'REQUEST_PORTFOLIO':
      return Object.assign({}, state, { isFetching: true });
    default:
      return state;
  }
}
