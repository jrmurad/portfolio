export default function notification(state = {}, action) {
  switch (action.type) {
    case 'CLEAR_NOTIFICATION':
      return Object.assign({}, state, { message: undefined });
    case 'DISPLAY_NOTIFICATION':
      return Object.assign({}, state, {
        bsStyle: action.bsStyle,
        message: action.message,
      });
    default:
      return state;
  }
}
