import reducer from './notification';

it('should return the initial state', () => {
  expect(reducer(undefined, {})).toEqual({ message: undefined });
});

it('should handle DISPLAY_NOTIFICATION', () => {
  expect(
    reducer({
      bsStyle: 'info',
      message: 'message1',
    }, {
      type: 'DISPLAY_NOTIFICATION',
      bsStyle: 'danger',
      message: 'message2',
    })
  ).toEqual({
    bsStyle: 'danger',
    message: 'message2',
  });
});

it('should handle CLEAR_NOTIFICATION', () => {
  expect(
    reducer({
      message: 'message1',
    }, {
      type: 'CLEAR_NOTIFICATION',
    })
  ).toEqual({
    message: undefined,
  });
});
