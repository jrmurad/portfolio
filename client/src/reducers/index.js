import { combineReducers } from 'redux';

import mainForm from './mainForm';
import notification from './notification';
import portfolio from './portfolio';
import quotes from './quotes';

const reducers = combineReducers({
  mainForm,
  notification,
  portfolio,
  quotes,
});

export default reducers;
