import reducer from './mainForm';

it('should return the initial state', () => {
  expect(reducer(undefined, {})).toEqual({ symbol: '' });
});

it('should handle SET_SYMBOL', () => {
  expect(
    reducer(
      { symbol: 'SYM1' },
      { type: 'SET_SYMBOL', symbol: 'SYM2' },
    )
  ).toEqual({ symbol: 'SYM2' });
});
