import reducer from './portfolio';

it('should return the initial state', () => {
  expect(reducer(undefined, {})).toEqual({ isFetching: true, stocks: [] });
});

it('should handle REQUEST_PORTFOLIO', () => {
  expect(
    reducer(undefined, {
      type: 'REQUEST_PORTFOLIO',
    })
  ).toEqual({
    isFetching: true,
    stocks: [],
  });
});

it('should handle RECEIVE_PORTFOLIO', () => {
  expect(
    reducer({
      isFetching: true,
      stocks: [1, 2, 3],
    }, {
      type: 'RECEIVE_PORTFOLIO',
      portfolio: [4, 5, 6],
    })
  ).toEqual({
    isFetching: false,
    stocks: [4, 5, 6],
  });
});
