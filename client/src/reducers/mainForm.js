export default function mainForm(state = {
  symbol: '',
}, action) {
  switch (action.type) {
    case 'SET_SYMBOL':
      return Object.assign({}, state, {
        symbol: action.symbol,
      });
    default:
      return state;
  }
}
