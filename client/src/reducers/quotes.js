function extendWithLastTick(quotes, state) {
  return quotes.reduce((map, quote) => {
    const symbol = quote.Symbol;

    let lastTick;

    if (state[symbol]) {
      const prevPrice = state[symbol].LastTradePriceOnly;
      const currPrice = quote.LastTradePriceOnly;
      lastTick = currPrice - prevPrice;
    }

    return { ...map, [symbol]: Object.assign({ lastTick }, quote) };
  }, {});
}

export default function (state = {}, action) {
  switch (action.type) {
    case 'RECEIVE_QUOTES': {
      return Object.assign({}, state, { ...extendWithLastTick(action.quotes, state) });
    }
    default:
      return state;
  }
}
