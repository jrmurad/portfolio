import bodyParser from 'body-parser';
import config from 'config';
import express from 'express';

import db from './lib/db';

const app = express();
app.use(bodyParser.json());

function handleErrors(res, err) {
  res
    .status(500)
    .send({
      error: err.errors.map(e => {
        // some errors (e.g. Validation) have an associated path
        if (e.path) {
          return `${e.path}: ${e.message}`;
        }

        return e.message;
      }).join()
    });
}

app.get('/api/portfolio', (req, res) => {
  db.Stock
    .findAll({ raw: true })
    .then(stocks => res.send(stocks))
    .catch(handleErrors.bind(this, res));
});

app.post('/api/portfolio/add', (req, res) => {
  // FIXME use a queue to atomicize count+insert
  db.Stock
    .count()
    .then((count) => {
      if (count >= config.maxPortfolioEntries) {
        return Promise.reject({
          errors: [{
            message: `max portfolio size is ${config.maxPortfolioEntries}`,
          }],
        });
      }

      return Promise.resolve();
    })
    .then(() => db.Stock.create(req.body))
    .then(stock => res.json({ stock }))
    .catch(handleErrors.bind(this, res));
});

app.delete('/api/portfolio/remove', (req, res) => {
  db.Stock
    .destroy({ where: { id: req.body.id } })
    .then(n => res.json({ n }))
    .catch(handleErrors.bind(this, res));
});

db.sequelize.sync().then(() => {
  app.listen(config.apiServerPort);
});

export default app;
