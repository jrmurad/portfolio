export default (sequelize, DataTypes) =>
  sequelize.define('Stock', {
    numSharesOwned: DataTypes.INTEGER.UNSIGNED,
    symbol: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
  }, {
    hooks: {
      beforeValidate: (stock) => {
        if (typeof stock.symbol === 'string') {
          stock.symbol = stock.symbol.trim().toUpperCase();
        }
      },
    },
  });
