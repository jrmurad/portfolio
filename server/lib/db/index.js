import config from 'config';
import fs from 'fs';
import path from 'path';
import Sequelize from 'sequelize';

const modelsDir = path.join(__dirname, 'models');

const db = {};

db.sequelize = new Sequelize(config.get('db'));

fs
  .readdirSync(modelsDir)
  .forEach((file) => {
    const model = db.sequelize.import(path.join(modelsDir, file));
    db[model.name] = model;
  });

export default db;
